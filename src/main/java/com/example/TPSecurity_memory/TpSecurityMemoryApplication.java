package com.example.TPSecurity_memory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TpSecurityMemoryApplication {

	public static void main(String[] args) {
		SpringApplication.run(TpSecurityMemoryApplication.class, args);
	}

}
